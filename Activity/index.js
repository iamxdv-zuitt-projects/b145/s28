/*1. In the S29 folder, create an activity and images folder and an index.js file inside of it.
2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
	a. If the url is http://localhost:4000/, Send a response Welcome to Booking System
	b. If the url is http://localhost:4000/profile, Send a response Welcome to your profile!
	c. If the url is http://localhost:4000/courses, Send a response Here's our courses available
	d. If the url is http://localhost:4000/addcourse, Send a response Add a course to our resources
	e. If the url is http://localhost:4000/updatecourse, Send a response Update a course to our resources
	f. If the url is http://localhost:4000/archievecourse, Send a response Archive course to our resources

3. Test all the endpoints in Postman
4. Create a git repository named 29
5. Initialize a local git repository, add the remote link and push to got with the commit message of "Add activity code"
6. Add link in Boodle*/

let http = require('http');

http.createServer((request, response) => {

	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to Booking System')
	};


	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Welcome to your profile!')
	}

	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Heres our courses available')
	}

	if(request.url == "/addCourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Add a course to our resources')
	}

    if(request.url == "/updateCourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Update a course to our resources')
	}

    if(request.url == "/archieveCourse" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type' : 'text/plain'})
		response.end('Archive course to our resources')
	}


}).listen(4000);

console.log('Server running at port 4000');

